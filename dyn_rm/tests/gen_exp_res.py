import subprocess
import os
import filecmp

all_tests = [
    "connection",
    "event_loop",
    "graph",
    "setop_model",
    "submission",
    "task_graph_creation",
    "topology_creation",
    "vertex_model"
]


def generate_expected_results(tests):
    failed_tests = []
    # Run the Python file
    for test in tests:
        filename = os.path.join("test_files","mca_test_"+test+".py")
        expected_results_file = os.path.join("test_results_expected","mca_test_exp_res_"+test+".txt")
        result = subprocess.run(['python3', os.path.join(filename)], capture_output=True, text=True)

        # Check if the process was successful
        with open(expected_results_file, 'w') as file:
            file.write(result.stdout)

        if result.returncode == 0:
            print(f"Successfully generated expected results for: {test}")
        else:
            errors = ""
            if result.returncode != 0:
                errors+=" |non-zero returncode: "+str(result.returncode)+"|"
            print(f"Error running {test}: "+errors)
            print(result.stderr)

if __name__ == "__main__":
    generate_expected_results(all_tests)