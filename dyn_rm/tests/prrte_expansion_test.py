from dyn_rm.mca.system.modules.process_manager import PrrteProcessManagerModule
from dyn_rm.mca.callback.modules.pmix import PmixCallbackModule
from dyn_rm.mca.base.system.module import MCATaskModule, MCANodeModule, MCACoreModule, MCATopologyGraphModule,MCAPSetModule, MCAPSetopModule, MCAProcModule

from dyn_rm.util.constants import *
from pmix import *

import argparse
from time import sleep

prrte_instance = None

class PrrteTest():
    def __init__(self, verbosity):
        self.prrte_instance = PrrteProcessManagerModule(verbosity=verbosity)
        self.prrte_connection = PmixCallbackModule(verbosity=verbosity)

    def start_prrte(self, hosts, ssh_agent, ssh_params):

        # Create a topology graph
        topology = MCATopologyGraphModule("PRRTE_SYSTEM")
        for host in hosts.split(','):
            node = MCANodeModule(host)
            core = MCACoreModule("core_0")
            topology.run_service("ADD", "TOPOLOGY_OBJECTS", [node, core])
            topology.run_service("ADD", "CONTAINMENT_RELATION", node, [core])

        args = {}
        if None != ssh_agent:
            args['ssh_agent'] = ssh_agent
        if None != ssh_params:
            args['ssh_params'] = ssh_params
        # Launch PRRTE onto this topology graph
        if DYNRM_SUCCESS != self.prrte_instance.run_service("LAUNCH", "PM", topology, args):
            raise Exception("PRRTE launch failed!")

        # Connect to PRRTE Master
        info = self.prrte_instance.run_service("GET", "INFO", [DYNRM_PARAM_CONN_INFO])
        conn_params = info[DYNRM_PARAM_CONN_INFO][0][DYNRM_PARAM_CONN_PARAMS]
        conn_params[DYNRM_PARAM_CONN_SCHEDULER] = True

        if DYNRM_SUCCESS != self.prrte_connection.run_service("REQUEST", "CONNECTION", "PRRTE", 
                                                              self, None, conn_params):
            raise Exception("Connection attempt to PRRTE failed!")

    def expand_prrte(self, hosts, command):

        # Create a Task 
        task = MCATaskModule("expansion_task", command.split(' ')[0])
        task.run_service("SET", "TASK_EXECUTION_ARGUMENTS", command.split(' ')[1:])

        # Create topology graph and map processes to be launched
        topology = MCATopologyGraphModule("PRRTE_SYSTEM")
        procs = []
        for host in hosts.split(','):
            node = MCANodeModule(host)
            core = MCACoreModule("core_0")
            
            topology.run_service("ADD", "TOPOLOGY_OBJECTS", [node, core])
            topology.run_service("ADD", "CONTAINMENT_RELATION", node, [core])
            
            proc = MCAProcModule("proc_on_"+host, command)
            proc.run_service("SET", "CORE_ACCESS", [core])
            procs.append(proc)

        # Create an ADD PSetOp: 'empty_set' -> 'launch_pset'
        null_pset = MCAPSetModule("Null_PSet", [])
        null_pset.run_service("SET", "NAME", "")

        launch_pset = MCAPSetModule("Launch_PSet", procs)
        launch_pset.run_service("SET", "TASK", task)
        
        psetop = MCAPSetopModule("Launch_PSetOp", DYNRM_PSETOP_ADD, [null_pset], output=[launch_pset])
        psetop.run_service("SET", "ATTRIBUTE", DYNRM_CMD_PM_EXPAND, True)
        # Send cmd to apply PSetOp to PRRTE   
        rc = self.prrte_connection.run_service("SEND", "EVENT", "PRRTE", DYNRM_CMD_PSETOP_APPLY, psetop)
        if rc != DYNRM_SUCCESS:
            raise Exception("PRRTE Expansion failed with error ", rc) 

    def terminate_prrte(self):
        rc = self.prrte_instance.run_service("TERMINATE", "PM")
        if rc != DYNRM_SUCCESS:
            raise Exception("PRRTE Termination failed with error ", rc) 


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("start_hosts", help="comma-seperated list of host for initial PRRTE launch")
    parser.add_argument("expand_hosts", help="comma-seperated list of hosts added in PRRTE expansion")
    parser.add_argument("--ssh_agent", help="ssh agent to be used for daemon launch", type=str, default=None)
    parser.add_argument("--ssh_params", help="ssh params to be used for daemon launch", type=str, default=None)
    parser.add_argument("--sleep_duration", help="Number of seconds to sleep before terminating PRRTE", type=int, default=10)
    parser.add_argument("--verbosity", help="Verbosity for DynRM modules", type=int, default=0)
    args = parser.parse_args()
    
    prrte_test = PrrteTest(args.verbosity)
    
    print("Starting PRRTE on hosts "+args.start_hosts)
    prrte_test.start_prrte(args.start_hosts, args.ssh_agent, args.ssh_params)

    print("Expanding PRRTE to hosts "+args.expand_hosts)
    prrte_test.expand_prrte(args.expand_hosts, "python3 prrte_expand_test_app.py "+str(args.sleep_duration))

    print("Sleeping "+str(args.sleep_duration)+ " seconds before terminating PRRTE")
    sleep(args.sleep_duration)

    print("Terminating PRRTE")
    prrte_test.terminate_prrte()

    print("Done with PRRTE Expansion Test. À bientôt!")