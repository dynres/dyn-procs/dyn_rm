from dyn_rm.mca.base.system.module.system import MCASystemModule
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule

from os.path import join,dirname

def test1(module_class, object):

    print()
    print("Testing module "+str(module_class.__name__))
    system = MCASystemModule()
    system.run_service("REGISTER", "TOPOLOGY_CREATION_MODULE", module_class())
    system.run_service("CREATE", "SYSTEM_TOPOLOGY", module_class, object, dict())

    topo_graph = system.run_service("GET", "TOPOLOGY_GRAPH")

    topo_graph.run_service("PRINT", "TOPOLOGY_GRAPH")

    system.run_service("MCA", "SHUTDOWN")



if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'topology_creation'")
    print("================================================================================")


    test1(DefaultTopologyGraphCreationModule, join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"))

    print()
    print("================================================================================")
    print("Finished MCA Test 'topology_creation' successfully")
    print("================================================================================")
