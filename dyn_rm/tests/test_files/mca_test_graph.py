from dyn_rm.mca.base.graph.component.graph import MCAGraphComponent

from dyn_rm.mca.base.graph.module.graph import MCAGraphModule
from dyn_rm.mca.base.graph.module.edge import MCAEdgeModule
from dyn_rm.mca.base.graph.module.vertex import MCAVertexModule
from dyn_rm.mca.base.graph.module.graph_object import MCAGraphObjectModule

from dyn_rm.util.constants import *

def test1():
    print()
    print("=================== Start of MCA Test: 'Graph' =================")
    print()
    graph_comp = MCAGraphComponent()
    graph1 = MCAGraphModule()

    vertex1 = MCAVertexModule()

    print()
    print("############ Vertices ##########")
    print()
    print("Vertex1: setting name to 'v1'")
    rc = vertex1.run_service("SET", "GID", "v1")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: seting name to 'v1' failed with error ",rc)
        exit(1)
    print("Vertex1: setting name to 'v1' returned 'SUCCESS'")
    
    print()
    print("Vertex1: getting name")
    name = vertex1.run_service("GET", "GID")
    print("Vertex1: getting name returned: "+name)

    print()
    print("Vertex1: Setting attribute: 'attribute_key1' : 'attribute_val1'")    
    rc = vertex1.run_service("SET", "ATTRIBUTE", "attribute_key1", "attribute_val1")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: Setting attribute: 'attribute_key1' : 'attribute_val1' failed with error ",rc)
        exit(1)
    print("Vertex1: Setting attribute: 'attribute_key1' : 'attribute_val1' returned 'SUCCESS'")       
    
    print()
    print("Vertex1: Setting monitoring entry: 'timestamp1' : 'monitoring_entry1'")  
    rc = vertex1.run_service("ADD", "MONITORING_ENTRY", "timestamp1", "monitoring_entry1")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: Setting Setting monitoring entry: 'timestamp1' : 'monitoring_entry1' failed with error ",rc)
        exit(1)    
    print("Vertex1: Setting Setting monitoring entry: 'timestamp1' : 'monitoring_entry1' returned 'SUCCESS'")

    print()
    print("Vertex1: Setting monitoring entry: 'timestamp2' : 'monitoring_entry2'")
    rc = vertex1.run_service("ADD", "MONITORING_ENTRY", "timestamp2", "monitoring_entry2")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: Setting Setting monitoring entry: 'timestamp2' : 'monitoring_entry2' failed with error ",rc)
        exit(1)    
    print("Vertex1: Setting Setting monitoring entry: 'timestamp2' : 'monitoring_entry2' returned 'SUCCESS'")
    
    print()
    print("Vertex1: Getting monitoring entry1")
    monitoring_entry1 = vertex1.run_service("GET", "MONITORING_ENTRY", "timestamp1")
    print("Vertex1: Getting Setting monitoring entry1 returned: "+monitoring_entry1)
    
    print()
    print("Vertex1: Removing monitoring entry1")
    rc = vertex1.run_service("REMOVE", "MONITORING_ENTRY", "timestamp1")
    print("Vertex1:Removing monitoring entry1 returned 'SUCCESS'")
    
    print()
    print("Vertex1: Getting monitoring data")
    monitoring_data = vertex1.run_service("GET", "MONITORING_DATA")
    print("Vertex1: Getting monitoring data returned "+str(monitoring_data))

    print()
    print("Creating Vertices 'v2' and 'v3'")

    vertex2 = MCAVertexModule()
    rc = vertex2.run_service("SET", "GID", "v2")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex2: Setting Name to 'v2' failed with error ",rc)
        exit(1)   

    vertex3 = MCAVertexModule()
    rc = vertex3.run_service("SET", "GID", "v3")
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex3: Setting Name to 'v3' failed with error ",rc)
        exit(1)   

    print()
    print()
    print("############ Edges ##########")
    print()

    e1 = MCAEdgeModule()
    print()
    print("Edge1: Setting name to 'e1'")
    rc = e1.run_service("SET", "GID", "e1")
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge1: Setting name to 'e1' failed with error ",rc)
        exit(1)
    print("Edge1: Setting name to 'e1' returned 'SUCCESS'")

    print()
    print("Edge1: Setting input to '[v1]'")
    rc = e1.run_service("SET", "INPUT", [vertex1])
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge1: Setting input to '[v1]' failed with error ",rc)
        exit(1)
    print("Edge1: Setting input to '[v1]' returned 'SUCCESS'")

    print()
    print("Edge1: Setting output to '[v2,v3]'")
    rc = e1.run_service("SET", "OUTPUT", [vertex2, vertex3])
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge1: Setting input to '[v1]' failed with error ",rc)
        exit(1)
    print("Edge1: Setting input to '[v1]' returned 'SUCCESS'")

    print()
    print("Edge1: Setting ATTRIBUTE: 'e1_attribute_key' : 'e1_attribute_val'")
    rc = e1.run_service("SET", "ATTRIBUTE", "e1_attribute_key", "e1_attribute_val")
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge1: Setting ATTRIBUTE: 'e1_attribute_key' : 'e1_attribute_val' failed with error ",rc)
        exit(1)
    print("Edge1: Setting ATTRIBUTE: 'e1_attribute_key' : 'e1_attribute_val' returned 'SUCCESS'")

    print()
    print("Setting up edge 'e2'")
    e2 = MCAEdgeModule()
    rc = e2.run_service("SET", "GID", "e2")
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge2: 'SET' 'NAME' 'e2' failed with  ",rc)
        exit(1)
    rc = e2.run_service("SET", "INPUT", [vertex3])
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge2: 'SET' 'INPUT' '[vertex3]' failed with  ",rc)
        exit(1)
    rc = e2.run_service("SET", "OUTPUT", [vertex1])
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge2: 'SET' 'OUTPUT' '[vertex1]' failed with  ",rc)
        exit(1)
    rc = e2.run_service("SET", "ATTRIBUTE", "e2_attribute_key", "e2_attribute_val")
    if rc != DYNRM_MCA_SUCCESS:
        print("Edge2: Setting ATTRIBUTE: 'e2_attribute_key' : 'e2_attribute_val' failed with error ",rc)
        exit(1)

    print()
    print("Setting up edge in vertices")
    rc = vertex1.run_service("ADD", "OUT_EDGE", e1)
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: 'ADD' 'OUT_EDGE' 'e1' failed with  ",rc)
        exit(1)
    rc = vertex1.run_service("ADD", "IN_EDGE", e2)
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex1: 'ADD' 'IN_EDGE' 'e2' failed with  ",rc)
        exit(1)
    rc = vertex2.run_service("ADD", "IN_EDGE", e1)
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex2: 'ADD' 'IN_EDGE' 'e1' failed with  ",rc)
        exit(1)
    rc = vertex3.run_service("ADD", "IN_EDGE", e1)
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex3: 'ADD' 'IN_EDGE' 'e1' failed with  ",rc)
        exit(1)
    rc = vertex3.run_service("ADD", "OUT_EDGE", e2)
    if rc != DYNRM_MCA_SUCCESS:
        print("Vertex3: 'ADD' 'OUT_EDGE' 'e2' failed with  ",rc)
        exit(1)


    edge = vertex1.run_service("GET", "EDGE", "e1")
    print("Vertex1 get edge e1: "+edge.run_service("GET", "GID"))
    edges = vertex1.run_service("GET", "EDGES")
    print("Vertex1 get edges: "+str([edge.run_service("GET", "GID") for edge in edges]))
    edges = vertex1.run_service("GET", "IN_EDGES")
    print("Vertex1 get in_edges: "+str([edge.run_service("GET", "GID")for edge in edges]))
    edges = vertex1.run_service("GET", "OUT_EDGES")
    print("Vertex1 get out_edges: "+str([edge.run_service("GET", "GID")for edge in edges]))

    def filter1(arg):
        return None != arg.run_service("GET", "ATTRIBUTE", "e1_attribute_key")
    edges = vertex1.run_service("GET", "EDGES_BY_FILTER", filter1)
    print("Vertex1 edge filter e1_attribute_key: "+str([edge.run_service("GET", "GID")for edge in edges]))
    def filter2(arg):
        return -1 < arg.run_service("GET", "INDEX_IN_INPUT", "v3")
    edges = vertex1.run_service("GET", "EDGES_BY_FILTER", filter2)
    print("Vertex1 edge filter 'v3' in INPUT': "+str([edge.run_service("GET", "GID")for edge in edges]))

    print()
    print()
    print("############ Graphs ##########")
    print()
    graph_comp = MCAGraphComponent()
    graph1 = MCAGraphModule()
    graph1.run_service("SET", "GID", "g1")
    graph2 = MCAGraphModule()
    graph2.run_service("SET", "GID", "g2")

    graph_comp.run_service("ADD", "GRAPH", "g1", graph1)
    graph_comp.run_service("ADD", "GRAPH", "g2", graph2)

    print()
    print("Graph1: Adding vertices '[vertex1, vertex2, vertex3]'")
    rc = graph1.run_service("ADD", "GRAPH_VERTICES", [vertex1, vertex2, vertex3])
    if rc != DYNRM_MCA_SUCCESS:
        print("Graph1: Adding vertices '[vertex1, vertex2, vertex3]' failed with  ",rc)
        exit(1)
    
    print()
    print("Graph1: Adding Edges '[e1, e2]'")
    graph1.run_service("ADD", "GRAPH_EDGES", [e1, e2])

    print()
    print("Graph1: Get vertex 'v1'")
    vertex = graph1.run_service("GET", "GRAPH_VERTEX", "v1")
    print("Graph1: Get vertex 'v1' returned "+vertex.run_service("GET", "GID"))

    def filter3(arg):
        return arg.run_service("GET", "GID") == "v1"
    print()
    print("Graph1: Get vertices-by_filter name=='v1'")
    vertices = graph1.run_service("GET", "GRAPH_VERTICES_BY_FILTER", filter3)
    print("Graph1: Get vertices-by_filter name=='v1' returned "+str([vertex.run_service("GET", "GID") for vertex in vertices]))

    print()
    print("Graph1: Get edges-by_filter 'e1_attribute_key'")
    edges = graph1.run_service("GET", "GRAPH_EDGES_BY_FILTER", filter1)
    print("Graph1: Get edges-by_filter 'e1_attribute_key' returned "+str([edge.run_service("GET", "GID") for edge in edges]))

    print()
    print("graph1: 'GET', 'ALL_GRAPH_VERTICES'")
    g1_vertices = graph1.run_service("GET", "ALL_GRAPH_VERTICES")
    print("graph1: 'GET', 'ALL_GRAPH_VERTICES' returned "+str([v.run_service("GET", "GID") for v in g1_vertices]))
    for vertex in g1_vertices:
        in_edges = vertex.run_service("GET", "IN_EDGES")
        out_edges = vertex.run_service("GET", "OUT_EDGES")
        print("vertex "+vertex.run_service("GET", "GID")+":")
        print("==> In_Edges: "+str([edge.run_service("GET", "GID") for edge in in_edges]))
        print("==> Out_Edges: "+str([edge.run_service("GET", "GID") for edge in out_edges]))

    print()
    print("graph1: 'GET', 'ALL_GRAPH_EDGES'")
    g1_edges = graph1.run_service("GET", "ALL_GRAPH_EDGES")
    print("graph1: 'GET', 'ALL_GRAPH_EDGES' returned "+str([e.run_service("GET", "GID") for e in g1_edges]))
    for edge in g1_edges:
        input = edge.run_service("GET", "INPUT")
        output = edge.run_service("GET", "OUTPUT")
        print("edge "+edge.run_service("GET", "GID")+":")
        print("==> Input: "+str([vertex.run_service("GET", "GID") for vertex in input]))
        print("==> Output: "+str([vertex.run_service("GET", "GID") for vertex in output]))

    print()
    print()
    print("############ Vertex Copy ##########")
    print()
    
    print()
    print("Vertex1: GET COPY")
    v1_copy = MCAVertexModule()
    v1_copy = vertex1.run_service("GET", "COPY", v1_copy)
    v1_copy.run_service("SET", "STATUS", MCAGraphObjectModule.STATUS_UPDATE)
    print("Vertex1: GET COPY returned: name="+v1_copy.run_service("GET", "GID")+" "+str(v1_copy.run_service("GET", "STATUS")))


    print()
    print("Vertex1_copy: REMOVE EDGE 'e1'")
    rc = v1_copy.run_service("REMOVE", "EDGE", "e1")
    if rc == None:
        print("Vertex1_copy: REMOVE EDGE 'e1' failed. Returned 'None'")
        exit(1)


    print()
    print("graph1: 'UPDATE' 'GRAPH' [v1_copy], []")
    rc = graph1.run_service("UPDATE", "GRAPH", [v1_copy])
    if rc != DYNRM_MCA_SUCCESS:
        print("graph1: 'UPDATE' 'GRAPH' [v1_copy], [] failed with ", rc)
        exit(1)
    print("graph1: 'UPDATE' 'GRAPH' [v1_copy], [] returned 'SUCCESS'")

    print()
    print("graph1: 'GET', 'ALL_GRAPH_VERTICES'")
    g1_vertices = graph1.run_service("GET", "ALL_GRAPH_VERTICES")
    print("graph1: 'GET', 'ALL_GRAPH_VERTICES' returned "+str([v.run_service("GET", "GID") for v in g1_vertices]))
    for vertex in g1_vertices:
        in_edges = vertex.run_service("GET", "IN_EDGES")
        out_edges = vertex.run_service("GET", "OUT_EDGES")
        print("vertex "+vertex.run_service("GET", "GID")+":")
        print("==> In_Edges: "+str([edge.run_service("GET", "GID") for edge in in_edges]))
        print("==> Out_Edges: "+str([edge.run_service("GET", "GID") for edge in out_edges]))

    print()
    print("graph1: 'GET', 'ALL_GRAPH_EDGES'")
    g1_edges = graph1.run_service("GET", "ALL_GRAPH_EDGES")
    print("graph1: 'GET', 'ALL_GRAPH_EDGES' returned "+str([e.run_service("GET", "GID") for e in g1_edges]))
    for edge in g1_edges:
        input = edge.run_service("GET", "INPUT")
        output = edge.run_service("GET", "OUTPUT")
        print("edge "+edge.run_service("GET", "GID")+":")
        print("==> Input: "+str([vertex.run_service("GET", "GID") for vertex in input]))
        print("==> Output: "+str([vertex.run_service("GET", "GID") for vertex in output]))



if __name__ == "__main__":
    test1()






