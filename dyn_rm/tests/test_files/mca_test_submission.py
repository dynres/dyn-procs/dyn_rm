from dyn_rm.mca.base.submission.component import MCASubmissionComponent
from dyn_rm.mca.base.submission.module import MCASubmissionModule

from dyn_rm.util.constants import *

import time

from os.path import join,dirname


def _object_submitted_callback(module_class, object, params):
    print("==> : OBJECT "+str(object)+" submitted by MODULE "+str(module_class.__name__)+" with params "+str(params))
    return DYNRM_SUCCESS

def test1(module, object, mix):

    print()
    print("===== Running Test with module "+str(module.__name__)+" =====")

    sc = MCASubmissionComponent()

    print()
    print("Testing 'GET' 'PARAMETERS'")
    rc = sc.run_service("SET", "PARAMETERS", {"key1" : "val1", "key2" : "val2"})
    if rc != DYNRM_MCA_SUCCESS:
        print("'SET', 'PARAMETERS' failed with ", rc)
        exit(1)

    print()
    print("Testing 'UNSET' 'PARAMETER'")
    rc = sc.run_service("UNSET", "PARAMETER", "key1")
    if rc != DYNRM_MCA_SUCCESS:
        print("'UNSET', 'PARAMETERS' failed with ", rc)
        exit(1)

    print()
    print("Testing 'GET' 'PARAMETERS'")
    print("==> "+str(sc.run_service("GET", "PARAMETERS")))
    
    print()
    print("Testing 'REGISTER' 'SUBMISSION_MODULE'")
    rc = sc.run_service("REGISTER", "SUBMISSION_MODULE", module())
    if rc != DYNRM_MCA_SUCCESS:
        print("'REGISTER', 'SUBMISSION_MODULE' failed with ", rc)
        exit(1)

    print()
    print("Testing 'SET' 'MODULE_PARAMETERS'")
    rc = sc.run_service("SET", "MODULE_PARAMETERS", module, {"key3" : "val3", "key4" : "val4"})
    if rc != DYNRM_MCA_SUCCESS:
        print("'SET', 'MODULE_PARAMETERS' failed with ", rc)
        exit(1)

    print()
    print("Testing 'UNSET' 'MODULE_PARAMETERS'")
    rc = sc.run_service("UNSET", "MODULE_PARAMETER", module, "key3")
    if rc != DYNRM_MCA_SUCCESS:
        print("'SET', 'MODULE_PARAMETERS' failed with ", rc)
        exit(1)

    print()
    print("Testing 'GET' 'MODULE_PARAMETERS'")
    print("==> "+str(sc.run_service("GET", "MODULE_PARAMETERS", module)))

    print()
    print("Testing 'REGISTER' 'OBJECT_SUBMITTED_CALLBACK'")
    rc = sc.run_service("REGISTER", "OBJECT_SUBMITTED_CALLBACK", _object_submitted_callback)
    if rc != DYNRM_MCA_SUCCESS:
        print("'REGISTER', 'OBJECT_SUBMITTED_CALLBACK' failed with ", rc)
        exit(1)

    print()
    print("Testing 'SUBMIT' 'OBJECT'")
    rc = sc.run_service("SUBMIT", "OBJECT", module, MCASubmissionComponent.LOOPBACK, object, {"param1": "val"}) 
    if rc != DYNRM_MCA_SUCCESS:
        print("'SUBMIT', 'OBJECT' failed with ", rc)
        exit(1)
    
    print()
    print("Testing 'SUBMIT' 'MIX'")
    rc = sc.run_service("SUBMIT", "MIX", module, MCASubmissionComponent.LOOPBACK, mix, {"param1": "val"})
    if rc != DYNRM_MCA_SUCCESS:
        print("'SUBMIT', 'MIX' failed with ", rc)
        exit(1)

    time.sleep(10)

    print()
    print("Testing 'SHUTDOWN' 'SUBMISSION_COMPONENT'")
    rc = sc.run_service("SHUTDOWN", "SUBMISSION_COMPONENT")
    if rc != DYNRM_MCA_SUCCESS:
        print("'SHUTDOWN', 'SUBMISSION_COMPONENT' failed with ", rc)
        exit(1)
    
    print()
    print("===== Finished Test with module "+str(module.__name__)+" =====")


if __name__ == "__main__":

    print()
    print("================================================================================")
    print("Starting MCA Test 'topology_creation'")
    print("================================================================================")


    test1(MCASubmissionModule,
        join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission.py"), 
        join(dirname(dirname(__file__)), "test_input/submission/mix.csv") 
    )

    print()
    print("================================================================================")
    print("Finished MCA Test 'topology_creation' successfully")
    print("================================================================================")

    