from dyn_rm.util.constants import *
from dyn_rm.mca.mca import MCAClass
from dyn_rm.mca.base.callback.component import MCACallbackComponent
from dyn_rm.mca.callback.modules import DefaultCallbackModule

import sys

class MyClass():
    def __init__(self):
        self.name = "myclass"
    def peer1_event1_conn1_cbfunc(self, conn_name, event_name, data = None):
        print("==> Peer1: "+str(self.name)+".peer1_event1_conn1_cbfunc called! conn="+conn_name+" event="+event_name+" data=" +str(data))
        return DYNRM_MCA_SUCCESS
    def peer2_event1_conn2_cbfunc(self, conn_name, event_name, data = None):
        print("==> Peer2: "+str(self.name)+".peer2_event1_conn2_cbfunc called! conn="+conn_name+" event="+event_name+" data=" +str(data))
        return DYNRM_MCA_SUCCESS

def peer1_event1_default_cbfunc(conn_name, event_name, data = None):
    print("==> Peer1: peer1_event1_default_cbfunc called! conn="+conn_name+" event="+event_name+" data=" +str(data))
    return DYNRM_MCA_SUCCESS
def peer2_event1_default_cbfunc(conn_name, event_name, data = None):
    print("==> Peer2: peer2_event1_default_cbfunc called! conn="+conn_name+" event="+event_name+" data=" +str(data))
    return DYNRM_MCA_SUCCESS

print()
print()
print("=========== CONNECTION TESTS: START =============")
print()
print()

peer1 = MCAClass()
peer2 = MCAClass()

peer1_conn_component = MCACallbackComponent(verbosity=5)
peer2_conn_component = MCACallbackComponent(verbosity=5)

peer1_conn_module = DefaultCallbackModule(verbosity=5)
peer2_conn_module = DefaultCallbackModule(verbosity=5)

peer1_conn_component.register_module(peer1_conn_module)
peer2_conn_component.register_module(peer2_conn_module)

peer1.register_component(peer1_conn_component)
peer2.register_component(peer2_conn_component)

print("############ ESTABLISH CONNECTIONS ###############")
print("Peer1: request connection 'conn1' with peer2")
rc = peer1.run_component_service(MCACallbackComponent, "REQUEST", "CONNECTION", DefaultCallbackModule, "test_conn1", peer1, peer2, None)
if rc != DYNRM_MCA_SUCCESS:
    print("Peer1: request connection 'conn1' with peer2 failed with ", rc)
    exit(1)
print("Peer2: request connection 'conn2' with peer1")
rc = peer2.run_component_service(MCACallbackComponent, "REQUEST", "CONNECTION", DefaultCallbackModule, "test_conn2", peer2, peer1, None)
if rc != DYNRM_MCA_SUCCESS:
    print("Peer2: request connection 'conn2' with peer1 failed with ", rc)
    exit(1)

############ REGISTER CONNCTION CALLBACKS ###############
print()
print("############ REGISTER CONNECTION CALLBACKS ###############")
my_class = MyClass()
print("peer1 register connection callback for 'event1' on 'conn1'")
rc = peer1.run_component_service(MCACallbackComponent, "REGISTER", "CONNECTION_CALLBACK", "test_conn1", "event1", my_class.peer1_event1_conn1_cbfunc) 
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 register connection callback for 'event1' on 'conn1' failed with ", rc)
    exit(1)
print("peer2 register connection callback for 'event1' on 'conn2'")
rc = peer2.run_component_service(MCACallbackComponent, "REGISTER", "CONNECTION_CALLBACK", "test_conn2", "event1", my_class.peer2_event1_conn2_cbfunc) 
if rc != DYNRM_MCA_SUCCESS:
    print("peer2 register connection callback for 'event1' on 'conn2' failed with ", rc)
    exit(1)

############ REGISTER DEFAULT CALLBACKS ###############
print()
print("############ REGISTER DEFAULT CALLBACKS ###############")
print("peer1 register default callback for 'event3'")
rc = peer1.run_component_service(MCACallbackComponent, "REGISTER", "CALLBACK", "event3", peer1_event1_default_cbfunc) 
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 register default callback for 'event1' failed with ", rc)
    exit(1)
print("peer2 register default callback for 'event3'")
peer2.run_component_service(MCACallbackComponent, "REGISTER", "CALLBACK", "event3", peer2_event1_default_cbfunc) 
if rc != DYNRM_MCA_SUCCESS:
    print("peer2 register default callback for 'event1' failed with ", rc)
    exit(1)

print()
print("############ PEER 1 SEND EVENTS ###############")
print("peer1 send event 'event1' on 'conn1'")
rc = peer1.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn1", "event1", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 send event 'event1' on 'conn1' failed with ", rc)
    exit(1)
print("peer1 send event 'event1' on 'conn2'")
rc = peer1.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn2", "event1", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 send event 'event1' on 'conn2' failed with ", rc)
    exit(1)
print("peer1 send event 'event3' on 'conn2'")
rc = peer1.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn1", "event3", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 send event 'event1' on 'conn2' failed with ", rc)
    exit(1)
print("peer1 bcast event 'event1'")
rc = peer1.run_component_service(MCACallbackComponent, "BCAST", "EVENT", "event1", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 bcast event 'event1' failed with ", rc)
    exit(1)

############ PEER 2 SEND EVENTS ###############
print()
print("############ PEER 2 SEND EVENTS ###############")
print("peer2 send event 'event2' on 'conn1'")
rc = peer2.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn1", "event2", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer2 send event 'event2' on 'conn1' failed with ", rc)
    exit(1)
print("peer2 send event 'event2' on 'conn2'")
rc = peer2.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn2", "event2", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer2 send event 'event2' on 'conn2' failed with ", rc)
    exit(1)
print("peer1 send event 'event3' on 'conn2'")
rc = peer2.run_component_service(MCACallbackComponent, "SEND", "EVENT", "test_conn2", "event3", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer1 send event 'event1' on 'conn2' failed with ", rc)
    exit(1)
print("peer2 bcast event 'event2'")
rc = peer2.run_component_service(MCACallbackComponent, "BCAST", "EVENT", "event2", {"data": "test"})
if rc != DYNRM_MCA_SUCCESS:
    print("peer2 bcast event 'event2' failed with ", rc)
    exit(1)

print()
print()
print("=========== CONNECTION TESTS: SUCCESS =============")
