from dyn_rm.mca.base.system.module import *
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.mca.system.modules.tasks import DefaultTaskGraphCreationModule
from dyn_rm.mca.submission.modules.default import DefaultSubmissionModule
from dyn_rm.mca.base.resource_manager.module import MCAResourceManagerModule

from dyn_rm.mca.policy.modules.discrete_steepest_ascend import Discrete_Steepest_Ascend

from os.path import join,dirname

def test1(module_class, topo_file, submission_file):

    print("Starting test for module "+module_class.__name__)

    resource_manager = module_class()

    resource_manager.run_service("ADD", "POLICY", "discrete_steepest_ascend", Discrete_Steepest_Ascend())
    
    resource_manager.run_service("ADD", "SYSTEM", "my_system", MCASystemModule, DefaultTopologyGraphCreationModule, topo_file)

    resource_manager.run_service("SUBMIT", "OBJECT", "my_system", DefaultSubmissionModule, submission_file, {"task_graph_creation_modules" : [DefaultTaskGraphCreationModule]})

    system = resource_manager.run_service("GET", "SYSTEM", "my_system")

    topo_graph = system.run_service("GET", "TOPOLOGY_GRAPH")
    task_graphs = system.run_service("GET", "TASK_GRAPHS")
    pset_graphs = system.run_service("GET", "PSET_GRAPHS")

    print("Printing Topology Graph:")
    topo_graph.run_service("PRINT", "TOPOLOGY_GRAPH")

    print("Printing Task Graphs:")
    for task_graph in task_graphs:
        task_graph.run_service("PRINT", "TASK_GRAPH")

    print("Printing PSet Graph:")
    for pset_graph in pset_graphs:
        pset_graph.run_service("PRINT", "PSET_GRAPH")
    

    resource_manager.run_service("MCA", "SHUTDOWN")

    print("Finished Test for module "+module_class.__name__)

if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'resource_manager'")
    print("================================================================================")


    test1(MCAResourceManagerModule, 
          join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"),
          join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission.py")
    )

    print()
    print("================================================================================")
    print("Finished MCA Test 'resource_manager' successfully")
    print("================================================================================")
