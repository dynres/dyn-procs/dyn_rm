from dyn_rm.mca.base.system.module.psets.pset import MCAPSetModule
from dyn_rm.mca.base.system.module.psets.proc import MCAProcModule
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel 


set_model = AmdahlPsetModel(None, enable_output=False, verbosity=1)
print("**************************")

print("Adding measurement 1: 'p': 48, 'iteration_time': 1080.85052419")
measurement1 = {"p": 48, "iteration_time": 1080.85052419}
set_model.run_service("ADD", "MONITORING_DATA", measurement1)

print("Adding measurement 2: 'p': 96, 'iteration_time': 1032.93477535 ")
measurement2 = {"p": 96, "iteration_time": 1032.93477535} 
set_model.run_service("ADD", "MONITORING_DATA", measurement2)

print("Adding measurement 3: 'p': 192, 'iteration_time': 1005.5113492 ")
measurement3 = {"p": 192, "iteration_time": 1005.5113492} 
set_model.run_service("ADD", "MONITORING_DATA", measurement3)

print("Adding measurement 4: 'p': 384, 'iteration_time': 993.202171326 ")
measurement4 = {"p": 384, "iteration_time": 993.202171326} 
set_model.run_service("ADD", "MONITORING_DATA", measurement4)

print("Adding measurement 5: 'p': 768, 'iteration_time': 987.738002062 ")
measurement5 = {"p": 768, "iteration_time": 987.738002062} 
set_model.run_service("ADD", "MONITORING_DATA", measurement5)

print("**************************")
print("Infering Model Parameters:")
set_model.run_service("EVAL", "MODEL_PARAMS")
model_params = set_model.run_service("GET", "MODEL_PARAMS")
print("   "+str(model_params))

procs = [MCAProcModule(str(i), "./mpi_program") for i in range (24) ] 
pset = MCAPSetModule("pset1", procs)

print("**************************")
print("Evaluating Speedup with 'p': 24")
speedup = set_model.run_service("EVAL", "VERTEX", pset, ["SPEEDUP"])
print("   "+str(speedup))

print("**************************")
print("Evaluating Runtime with 'p': 24")
runtime = set_model.run_service("EVAL", "VERTEX", pset, ["RUNTIME"])
print("   "+str(runtime))

print("**************************")
print("Evaluating Gradient with 'p': 24")
derivative = set_model.run_service("EVAL", "VERTEX_GRADIENT", pset, ["SPEEDUP", "RUNTIME"])
print("   "+str(derivative))


