
from dyn_rm.mca.base.system import *
from dyn_rm.mca.base.system.module.system import MCASystemModule
from dyn_rm.mca.base.system.module.psets.psetop import MCAPSetopModule
from dyn_rm.mca.base.system.module.psets.proc import MCAProcModule
from dyn_rm.mca.base.system.module.psets.pset import MCAPSetModule
from dyn_rm.mca.base.system.module.psets.pset_graph_object import MCAPsetGraphObject
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel
from dyn_rm.mca.system.modules.psets.psetop_models.grow.default import DefaultGrowModel
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.my_pmix_constants import *
from dyn_rm.util.constants import *

from os.path import join,dirname

nodes = []

def output_space_generator1(setop, input, nodes):

    input_procs = input[0].run_service("GET", "PROCS")

    # multiple of 2 constraint
    if ((len(input_procs) +len(nodes)) & ((len(input_procs) +len(nodes) - 1))) != 0:
        print("===> Multiple of 2 constraint not satisfied: "+str(len(input_procs) +len(nodes)))
        return [], []
    
    # minimum constraint
    if len(input_procs) + len(nodes) < 3:
        print("===> Minimum constraint not satisfied")
        return [], []
    
    # maximum constraint
    if len(input_procs) + len(nodes) > 7:
        print("===> Maximum constraint not satisfied")
        return [], []
    
    procs = []
    # one proc per node
    for index in range(len(nodes)):
        cores = nodes[index].run_service("GET", "CORES")
        proc = MCAProcModule(str(index), "./mpi_program")
        proc.run_service("SET", "CORE_ACCESS", cores)
        procs.append(proc)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 10, "t_p": 300})
    
    delta_pset = MCAPSetModule("delta", procs)
    grow_pset = MCAPSetModule("grow", input_procs + procs, "amdahl", amdahl_model)

    return [[delta_pset, grow_pset]], [[delta_pset, grow_pset]]





    
    


def test1(topo_file, num_cur_nodes, num_add_nodes):
    print()
    print()
    print("********************************************************************************")
    print("Starting Test: | topo_file = "+topo_file+" | num_cur_nodes =",num_cur_nodes," | num_add_nodes =",num_add_nodes, " |")
    print("********************************************************************************")
    print()

    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 10, "t_p": 300})

    print()
    print("###########################################################")
    print("Setting up the system")
    system = MCASystemModule()
    system.run_service("REGISTER", "TOPOLOGY_CREATION_MODULE", DefaultTopologyGraphCreationModule())
    system.run_service("CREATE", "SYSTEM_TOPOLOGY", DefaultTopologyGraphCreationModule, topo_file, dict())
    nodes = system.run_service("GET", "NODES")
    cores = system.run_service("GET", "CORES")
    print("###########################################################")

    print()
    print("###########################################################")
    print("Setting up the Input Pset")
    procs = [MCAProcModule(str(i), "./mpi_program") for i in range (1)]
    for index in range(len(procs)):
        procs[index].run_service("SET", "CORE_ACCESS", [cores[index * len(nodes[0].run_service("GET", "CORES"))]]) 
    input_pset = MCAPSetModule("input1", procs, "amdahl", amdahl_model)
    input = [input_pset]
    print("###########################################################")

    print()
    print("###########################################################")
    print("Setting up the Grow Pset Operation")
    grow_model = DefaultGrowModel()
    grow_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator1)
    setop = MCAPSetopModule("setop1", "job1", DYNRM_PSETOP_GROW, input)
    setop.run_service("ADD", "PSETOP_MODEL", "grow", grow_model)
    print("###########################################################")


    print()
    print("###########################################################")
    print("Generating Output Space")
    o, v = grow_model.run_service("GENERATE", "OUTPUT_SPACE", setop, input, nodes[num_cur_nodes:num_add_nodes + 1], False)
    print("###########################################################")


    print()
    print("###########################################################")
    print("Collapsing Output Space of size "+str(len(o)))
    output = grow_model.run_service("COLLAPS", "OUTPUT_SPACE", input, o, ["SPEEDUP"])
    print("###########################################################")

    print()
    print("###########################################################")
    print("Evaluating Set Operation "+str([s.run_service("GET", "NAME") for s in input])+" => "+str([s.run_service("GET", "NAME") for s in output]))
    speedup = grow_model.run_service("EVAL", "EDGE", input, output, ["SPEEDUP"])

    print()
    print("===> SPEEDUP = "+str(speedup))
    if len(output) > 1:
        print("===> PSET SIZE = "+str(output[1].run_service("GET", "NUM_PROCS")))
    print("###########################################################")

    system.run_service("MCA", "SHUTDOWN")
    





if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'setop'")
    print("================================================================================")


    test1(join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"), 1, 1)
    test1(join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"), 1, 2)
    test1(join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"), 1, 3)

    print()
    print("================================================================================")
    print("Finished MCA Test 'setop' successfully")
    print("================================================================================")


