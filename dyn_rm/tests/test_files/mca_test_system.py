from dyn_rm.mca.base.system.module import *
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.mca.system.modules.tasks import DefaultTaskGraphCreationModule
from dyn_rm.mca.system.modules.tasks import DefaultTaskGraphCreationModule


from os.path import join,dirname

def test1(module_class, topo_file, submission_file):

    print("Starting test for module "+module_class.__name__)

    system = module_class()

    topo_creation = DefaultTopologyGraphCreationModule()
    topo_graph = MCATopologyGraphModule("my_topology")
    topo_graph.run_service("SET", "GID", system.run_service("GET", "NEW_GID"))
    topo_graph = topo_creation.run_service("CREATE", "TOPOLOGY_GRAPH", topo_graph, topo_file, dict())

    task_creation = DefaultTaskGraphCreationModule()
    task_graph = MCATaskGraphModule("my_task_graph")
    task_graph.run_service("SET", "GID", system.run_service("GET", "NEW_GID"))
    task_graph = task_creation.run_service("CREATE", "TASK_GRAPH", task_graph, submission_file, dict())


    system.run_service("SET", "TOPOLOGY_GRAPH", topo_graph)
    system.run_service("SUBMIT", "TASK_GRAPH", task_graph)

    topo_graph = system.run_service("GET", "TOPOLOGY_GRAPH")
    task_graphs = system.run_service("GET", "TASK_GRAPHS")
    pset_graphs = system.run_service("GET", "PSET_GRAPHS")

    print("Printing Topology Graph:")
    topo_graph.run_service("PRINT", "TOPOLOGY_GRAPH")

    print("Printing Task Graphs:")
    for task_graph in task_graphs:
        task_graph.run_service("PRINT", "TASK_GRAPH")

    print("Printing PSet Graph:")
    for pset_graph in pset_graphs:
        pset_graph.run_service("PRINT", "PSET_GRAPH")
    
    system.run_service("MCA", "SHUTDOWN")

    print("Finished Test for module "+module_class.__name__)

if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'system'")
    print("================================================================================")


    test1(MCASystemModule, 
          join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"),
          join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission.py")
    )

    print()
    print("================================================================================")
    print("Finished MCA Test 'system' successfully")
    print("================================================================================")
