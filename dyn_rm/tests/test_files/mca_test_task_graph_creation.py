from dyn_rm.mca.system.modules.tasks.task_graph_creation import DefaultTaskGraphCreationModule
from dyn_rm.mca.base.system.module.tasks.task_graph import MCATaskGraphModule

from os.path import join,dirname

def test1(module_class, object):

    print()
    print("Testing module "+str(module_class.__name__))
    creator = module_class()
    graph = MCATaskGraphModule("submission_1")
    task_graph = creator.run_service("CREATE", "TASK_GRAPH", graph, object, dict())

    task_graph.run_service("PRINT", "TASK_GRAPH", dict())



if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'topology_creation'")
    print("================================================================================")


    test1(DefaultTaskGraphCreationModule, join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission.py"))

    print()
    print("================================================================================")
    print("Finished MCA Test 'topology_creation' successfully")
    print("================================================================================")
