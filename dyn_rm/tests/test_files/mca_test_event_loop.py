from dyn_rm.mca.base.event_loop.component import MCAEventLoopComponent
from dyn_rm.mca.event_loop.modules.asyncio import AsyncioEventLoopModule

from dyn_rm.util.constants import *

from time import sleep

cbdata1 = "cbdata1"
cbdata2 = "cbdata2"

def func1(text):
    print(" ==> Func1: input="+text)
    return text+"_func1"

def func2(text):
    print(" ==> Func2: input="+text)
    return text+"_func1"

def cbfunc1(text, cbdata = None):
    print(" ==> cbfunc1: input="+text+" cbdata="+cbdata)
    return text+"_cbfunc1"

def cbfunc2(text, cbdata = None):
    print(" ==> cbfunc2: input="+text+" cbdata="+cbdata)
    return text+"_cbfunc2"

def func_interact(other_loop_component, loop_name, text):
    return other_loop_component.run_service("RUN", "FUNC", loop_name, func1, text+"_interact")

event_loop_component1 = MCAEventLoopComponent(verbosity=5)
event_loop_component2 = MCAEventLoopComponent(verbosity=5)

event_loop_component1.register_module(AsyncioEventLoopModule(verbosity=5))
event_loop_component2.register_module(AsyncioEventLoopModule(verbosity=5))

print("================= STARTING MCA TEST 'EVENT_LOOP' ===================")

print()
print("********* Registering Event Loops ************")
print("Registering Event Loop 1")
rc = event_loop_component1.run_service("REGISTER", "EVENT_LOOP", AsyncioEventLoopModule, "loop1")
if rc != DYNRM_MCA_SUCCESS:
    print("Registering Event Loop 1 failed")
    exit(rc)

print("Registering Event Loop 2")
rc = event_loop_component1.run_service("REGISTER", "EVENT_LOOP", AsyncioEventLoopModule, "loop2")
if rc != DYNRM_MCA_SUCCESS:
    print("Registering Event Loop 1 failed")
    exit(rc)

print("Registering Event Loop 3 in component 2")
rc = event_loop_component2.run_service("REGISTER", "EVENT_LOOP", AsyncioEventLoopModule, "loop3")
if rc != DYNRM_MCA_SUCCESS:
    print("Registering Event Loop 3 failed")
    exit(rc)

print()
print("********* Starting Event Loops ************")
print("Starting Event Loop 1")
rc = event_loop_component1.run_service("START", "EVENT_LOOP", "loop1")
if rc != DYNRM_MCA_SUCCESS:
    print("Starting Event Loop 1 failed with "+str(rc))
    exit(rc)

print("Starting Event Loop 2")
rc = event_loop_component1.run_service("START", "EVENT_LOOP", "loop2")
if rc != DYNRM_MCA_SUCCESS:
    print("Starting Event Loop 2 failed")
    exit(rc)

print("Starting Event Loop 3 in Component 2")
rc = event_loop_component2.run_service("START", "EVENT_LOOP", "loop3")
if rc != DYNRM_MCA_SUCCESS:
    print("Starting Event Loop 3 failed")
    exit(rc)

print()
print("********* Testing RUN FUNC ************")
print("Running func1 in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC", "loop1", func1, "func1_input")
print("Running func1 in Event Loop 1 returned "+str(res))

print("Running func2 in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC", "loop2", func2, "func2_input")
print("Running func1 in Event Loop 1 returned "+str(res))


print()
print("********* Testing RUN FUNC_NB ************")
print("Running func1 non-blocking in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_NB", "loop1", cbfunc1, cbdata1, func1, "func1_nb_input")
print("Running func1 non-blocking in Event Loop 1 returned "+str(res))

print("Running func2 non-blocking in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_NB", "loop2", cbfunc2, cbdata2, func2, "func2_nb_input")
print("Running func2 non-blocking in Event Loop 2 returned "+str(res))

print()
print("********* Testing RUN FUNC_ASYNC ************")
print("Running func1 async in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_ASYNC", "loop1", func1, "func1_async_input")
print("Running func1 async in Event Loop 1 returned "+str(res))

print("Running func2 async in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_ASYNC", "loop2", func2, "func2_async_input")
print("Running func2 async in Event Loop 2 returned "+str(res))

print()
print("********* Testing RUN FUNC_NB_DELAYED (2 sec) ************")
print("Running func1 async non-blocking delayed (2sec) in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_NB_DELAYED", "loop1", 2, cbfunc1, cbdata1, func1, "func1_nb_delayed_input")
print("Running func1 async non-blocking delayed (2sec) in Event Loop 1 returned "+str(res))

print("Running func2 async non-blocking delayed (2sec) in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_NB_DELAYED", "loop2", 2, cbfunc2, cbdata2, func2, "func2_nb_delayed_input")
print("Running func2 async non-blocking delayed (2sec) in Event Loop 2 returned "+str(res))

print()
print("********* Testing RUN FUNC_DELAYED (1 sec) ************")
print("Running func1 delayed (1 sec) in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_DELAYED", "loop1", 1,func1, "func1_delayed_input")
print("Running func1 delayed (1 sec) in Event Loop 1 returned "+str(res))

print("Running func2 delayed (1 sec) in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_DELAYED", "loop2", 1, func2, "func2_delayed_input")
print("Running func2 delayed (1 sec) in Event Loop 2 returned "+str(res))

print()
print("********* Testing RUN FUNC_NB_ASYNC (1 sec) ************")
print("Running func1 non-blocking async in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_NB_ASYNC", "loop1", cbfunc1, cbdata1, func1, "func1_nb_async_input")
print("Running func1 non-blocking async in Event Loop 1 returned "+str(res))

print("Running func2 non-blocking async in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_NB_ASYNC", "loop2", cbfunc2, cbdata2, func2, "func2_nb_async_input")
print("Running func2 non-blocking async in Event Loop 2 returned "+str(res))

print()
print("********* Testing RUN FUNC_ASYNC_DELAYED (1 sec) ************")
print("Running func1 async delayed in Event Loop 1")
res = event_loop_component1.run_service("RUN", "FUNC_ASYNC_DELAYED", "loop1", 1, func1, "func1_async_delayed_input")
print("Running func1 async delayed in Event Loop 1 returned "+str(res))

print("Running func2 async delayed in Event Loop 2")
res = event_loop_component1.run_service("RUN", "FUNC_ASYNC_DELAYED", "loop2", 1, func2, "func2_async_delayed_input")
print("Running func2 async delayed in Event Loop 2 returned "+str(res))


print("Running func_interact in Event Loop 3 -> Event Loop 1")
res = event_loop_component2.run_service("RUN", "FUNC", "loop3", func_interact, event_loop_component1, "loop1", "func_interact_input")
print("Running func_interact in Event Loop 3 -> Event Loop 1 returned "+str(res))


print("Sleeping for 5 seconds")
sleep(5)



print()
print("********* Testing STOP EVENT_LOOP (1 sec) ************")
print("Stopping Event Loop 1")
res = event_loop_component1.run_service("STOP", "EVENT_LOOP", "loop1")
print("Stopping Event Loop 1 returned "+str(res))

print("Stopping Event Loop 2")
res = event_loop_component1.run_service("STOP", "EVENT_LOOP", "loop2")
print("Stopping Event Loop 2 returned "+str(res))

print("Stopping Event Loop 3")
res = event_loop_component2.run_service("STOP", "EVENT_LOOP", "loop3")
print("Stopping Event Loop 3 returned "+str(res))

print()
print("********* Testing DEREGISTER EVENT_LOOP (1 sec) ************")
print("Deregistering Event Loop 1")
res = event_loop_component1.run_service("DEREGISTER", "EVENT_LOOP", "loop1")
print("Deresgistering Event Loop 1 returned "+str(res))

print("Deregistering Event Loop 2")
res = event_loop_component1.run_service("DEREGISTER", "EVENT_LOOP", "loop2")
print("Deregistering Event Loop 2 returned "+str(res))

print("Deregistering Event Loop 3")
res = event_loop_component2.run_service("DEREGISTER", "EVENT_LOOP", "loop3")
print("Deregistering Event Loop 3 returned "+str(res))

print("Remaining Event Loops in comp1: "+str(event_loop_component1.event_loops))
print("Remaining Event Loops in comp2: "+str(event_loop_component2.event_loops))



print("================= FINISHED MCA TEST 'EVENT_LOOP' ===================")
