
from dyn_rm.mca.base.system import *
from dyn_rm.mca.base.system.module.system import MCASystemModule
from dyn_rm.mca.base.system.module.psets.psetop import MCAPSetopModule
from dyn_rm.mca.base.system.module.psets.proc import MCAProcModule
from dyn_rm.mca.base.system.module.psets.pset import MCAPSetModule
from dyn_rm.mca.base.system.module.psets.pset_graph_object import MCAPsetGraphObject
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel
from dyn_rm.mca.system.modules.psets.psetop_models.output_space_generators import *
from dyn_rm.mca.system.modules.psets.psetop_models import DefaultSplitModel,DefaultUnionModel
from dyn_rm.mca.system.modules.topology import DefaultTopologyGraphCreationModule
from dyn_rm.my_pmix_constants import *
from dyn_rm.util.constants import *

from os.path import join,dirname

nodes = []


def test1(topo_file, num_cur_nodes, num_add_nodes):
    print()
    print()
    print("********************************************************************************")
    print("Starting Test: | topo_file = "+topo_file+" | num_cur_nodes =",num_cur_nodes," | num_add_nodes =",num_add_nodes, " |")
    print("********************************************************************************")
    print()

    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 10, "t_p": 300})

    print()
    print("###########################################################")
    print("Setting up the system")
    system = MCASystemModule()
    system.run_service("REGISTER", "TOPOLOGY_CREATION_MODULE", DefaultTopologyGraphCreationModule())
    system.run_service("CREATE", "SYSTEM_TOPOLOGY", DefaultTopologyGraphCreationModule, topo_file, dict())

    print("###########################################################")

    print()
    print("###########################################################")
    print("Setting up the Input Pset")
    procs = [MCAProcModule(str(i), "./mpi_program") for i in range (32)]
    input_pset = MCAPSetModule("input1", procs, "amdahl", amdahl_model)
    input = [input_pset]
    print("###########################################################")

    print()
    print("###########################################################")
    print("Setting up the Split Pset Operation")
    split_model = DefaultSplitModel()
    split_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator_split)
    setop = MCAPSetopModule("setop1", "job1", DYNRM_PSETOP_SPLIT, input)
    setop.run_service("ADD", "PSETOP_MODEL", "USER_MODEL", split_model)
    print("###########################################################")


    print()
    print("###########################################################")
    print("Generating Output Space")
    o, v = split_model.run_service("GENERATE", "OUTPUT_SPACE", setop, input, None, False, parts="8,8,8,8")
    print("###########################################################")


    print()
    print("###########################################################")
    print("Generated output Space:")
    print(o, v)
    for pset in o:
        print([p.run_service("GET", "NAME") for p in pset.run_service("GET", "PROCS")])
    print("###########################################################")

    print()
    print("###########################################################")
    print("Setting up the Union Pset Operation")
    union_model = DefaultUnionModel()
    union_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator_union)
    setop = MCAPSetopModule("setop1", "job1", DYNRM_PSETOP_UNION, o)
    setop.run_service("ADD", "PSETOP_MODEL", "USER_MODEL", union_model)
    print("###########################################################")


    print()
    print("###########################################################")
    print("Generating Output Space")
    o, v = union_model.run_service("GENERATE", "OUTPUT_SPACE", setop, o, None, False)
    print("###########################################################")


    print()
    print("###########################################################")
    print("Generated output Space:")
    print(o, v)
    for pset in o:
        print([p.run_service("GET", "NAME") for p in pset.run_service("GET", "PROCS")])
    print("###########################################################")

    system.run_service("MCA", "SHUTDOWN")
    





if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'setop'")
    print("================================================================================")


    test1(join(dirname(dirname(__file__)), "test_input/topology/4_node_system.yaml"), 1, 1)


    print()
    print("================================================================================")
    print("Finished MCA Test 'setop' successfully")
    print("================================================================================")


