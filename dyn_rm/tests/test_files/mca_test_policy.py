from dyn_rm.mca.base.system.module import *
from dyn_rm.mca.system.modules.tasks.task_graph_creation import DefaultTaskGraphCreationModule
from dyn_rm.mca.system.modules.topology.default import DefaultTopologyGraphCreationModule
from dyn_rm.mca.policy.modules.discrete_steepest_ascend import Discrete_Steepest_Ascend
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel
from dyn_rm.mca.system.modules.psets.psetop_models.grow import DefaultGrowModel
from dyn_rm.mca.system.modules.system import PrrteSystem
from dyn_rm.util.constants import *

from os.path import join,dirname

from time import sleep

from copy import *



def get_delta_pset_for_nodes(nodes, task):
    procs = []
    # fill node
    for index in range(len(nodes)):
        cores = nodes[index].run_service("GET", "CORES")
        for core in cores:
            proc = MCAProcModule(str(index), task.run_service("GET", "TASK_EXECUTABLE"))
            proc.run_service("SET", "CORE_ACCESS", [core])
            procs.append(proc)
    
    delta_pset = MCAPSetModule("delta", procs)
    delta_pset.run_service("SET", "TASK", task)

    return delta_pset, procs

def output_space_generator1(setop, input, graphs, task=None):

    null_pset = input[0]
    null_pset_procs = null_pset.run_service("GET", "PROCS")
    print(null_pset.run_service("GET", "NAME")+" :"+str([p.run_service("GET", "GID") for p in null_pset_procs]))

    task = input[0].run_service("GET", "TASK")
    topology_graph = graphs["TOPOLOGY_GRAPH_ADD"]
    new_nodes = topology_graph.run_service("GET", "GRAPH_VERTICES_BY_FILTER", lambda x: isinstance(x, MCANodeModule))
    current_nodes = null_pset.run_service("GET", "ACCESSED_NODES")
    num_nodes = len(new_nodes) + len(current_nodes)
    # multiple of 2 constraint
    if (num_nodes & (num_nodes - 1)) != 0:
        print("===> Multiple of 2 constraint not satisfied: ", len(new_nodes), " ", len(current_nodes))
        return [], []
    print("===> Multiple of 2 constraint satisfied: ", len(new_nodes), " ", len(current_nodes))
    
    # minimum constraint
    if num_nodes < 1 or num_nodes > 4:
        print("===> Min/Max constraint not satisfied")
        return [], []
    
    delta_pset, procs = get_delta_pset_for_nodes(new_nodes, task)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 1, "t_p": 300})

    grow_pset = MCAPSetModule("grow1", procs + null_pset_procs)
    grow_pset.run_service("ADD", "PSET_MODEL", "USER_MODEL", amdahl_model)
    grow_pset.run_service("SET", "TASK", task)

    # Note: Lists of Lists => For each possibility return: output_lists and lists_of_adapted_objects 
    return [[delta_pset, grow_pset]], [[delta_pset, grow_pset] + procs]

# 1 proc per node, multiple of 2 and minimum 4 nodes
def output_space_generator2(setop, input, graphs, task=None):

    null_pset = input[0]
    null_pset_procs = null_pset.run_service("GET", "PROCS")
    task = input[0].run_service("GET", "TASK")
    topology_graph = graphs["TOPOLOGY_GRAPH_ADD"]
    new_nodes = topology_graph.run_service("GET", "GRAPH_VERTICES_BY_FILTER", lambda x: isinstance(x, MCANodeModule))
    current_nodes = null_pset.run_service("GET", "ACCESSED_NODES")
    num_nodes = len(new_nodes) + len(current_nodes)
    

    # minimum constraint
    if num_nodes < 1 or num_nodes > 5:
        print("===> Min/Max constraint not satisfied")
        return [], []
    
    delta_pset, procs = get_delta_pset_for_nodes(new_nodes, task)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 15, "t_p": 300})

    grow_pset = MCAPSetModule("grow1", procs + null_pset_procs)
    grow_pset.run_service("ADD", "PSET_MODEL", "USER_MODEL", amdahl_model)
    grow_pset.run_service("SET", "TASK", task)

    # Note: Lists of Lists => For each possibility return: output_lists and lists_of_adapted_objects 
    return [[delta_pset, grow_pset]], [[delta_pset, grow_pset] + procs]

# 1 proc per node, multiple of 2 and minimum 4 nodes
def output_space_generator3(setop, input, graphs, task=None):

    null_pset = input[0]
    null_pset_procs = null_pset.run_service("GET", "PROCS")
    task = input[0].run_service("GET", "TASK")
    topology_graph = graphs["TOPOLOGY_GRAPH_ADD"]
    new_nodes = topology_graph.run_service("GET", "GRAPH_VERTICES_BY_FILTER", lambda x: isinstance(x, MCANodeModule))
    current_nodes = null_pset.run_service("GET", "ACCESSED_NODES")
    num_nodes = len(new_nodes) + len(current_nodes)

    # minimum constraint
    if num_nodes < 1 or num_nodes > 2:
        print("===> Min/Max constraint not satisfied")
        return [], []
    
    delta_pset, procs = get_delta_pset_for_nodes(new_nodes, task)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 2, "t_p": 300})

    grow_pset = MCAPSetModule("grow1", procs + null_pset_procs)
    grow_pset.run_service("ADD", "PSET_MODEL", "USER_MODEL", amdahl_model)
    grow_pset.run_service("SET", "TASK", task)

    # Note: Lists of Lists => For each possibility return: output_lists and lists_of_adapted_objects 
    return [[delta_pset, grow_pset]], [[delta_pset, grow_pset] + procs]


def test1(policy_module, system_module, topo_file, submission_file):
    
    system = system_module()


    topo_creation = DefaultTopologyGraphCreationModule()
    topo_graph = MCATopologyGraphModule("my_topology")
    topo_graph.run_service("SET", "GID", system.run_service("GET", "NEW_GID"))
    topo_graph = topo_creation.run_service("CREATE", "TOPOLOGY_GRAPH", topo_graph, topo_file, dict())

    task_creation = DefaultTaskGraphCreationModule()
    task_graph = MCATaskGraphModule("my_task_graph")
    task_graph.run_service("SET", "GID", system.run_service("GET", "NEW_GID"))
    task_graph = task_creation.run_service("CREATE", "TASK_GRAPH", task_graph, submission_file, dict())


    system.run_service("SET", "TOPOLOGY_GRAPH", topo_graph)
    system.run_service("SUBMIT", "TASK_GRAPH", task_graph)


    system.run_service("PRINT", "SYSTEM")

    topo_graph = system.run_service("GET", "TOPOLOGY_GRAPH")
    task_graphs = system.run_service("GET", "TASK_GRAPHS")
    pset_graphs = system.run_service("GET", "PSET_GRAPHS")

    nodes = topo_graph.run_service("GET", "TOPOLOGY_OBJECTS", MCANodeModule)
    index = 0
    for pset_graph in pset_graphs:
        psetops = pset_graph.run_service("GET", "PSETOPS")
        for psetop in psetops:
            add_topo_graph = MCATopologyGraphModule("delta")
            add_topo_graph.run_service("ADD", "TOPOLOGY_OBJECTS", [nodes[index]])
            index += 1
            model = psetop.run_service("GET", "PSETOP_MODEL", "USER_MODEL")
            o, a = model.run_service("GENERATE", "OUTPUT_SPACE", 
                                    psetop, 
                                    psetop.run_service("GET", "INPUT"),
                                    {"TOPOLOGY_GRAPH_ADD": add_topo_graph,
                                     "TOPOLOGY_GRAPH_SUB": MCATopologyGraphModule("delta_sub"),
                                     "PSET_GRAPH" : pset_graph,
                                     "TASK_GRAPH" : task_graph
                                    },
                                    False,
            )

            system.run_service("APPLY", "PSETOPS", [psetop], [o[0]], [a[0]])
            system.run_service("FINALIZE", "PSETOP", psetop.run_service("GET", "GID"))

            task = o[0][0].run_service("GET", "TASK")
            grow_psetop = MCAPSetopModule("grow", DYNRM_PSETOP_GROW, o[0])
            grow_model = DefaultGrowModel()
            if task.run_service("GET", "NAME") == "task1":
                grow_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator1)
            elif task.run_service("GET", "NAME") == "task2":
                grow_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator2)
            elif task.run_service("GET", "NAME") == "task3":
                grow_model.run_service("SET", "OUTPUT_SPACE_GENERATOR", output_space_generator3)
            
            grow_psetop.run_service("ADD", "PSETOP_MODEL", "USER_MODEL", grow_model)
            
            system.run_service("ADD", "PSETOPS", [grow_psetop])


    system.run_service("PRINT", "SYSTEM")

    policy_module = policy_module(None, enable_output=False, verbosity=1)

    result = policy_module.run_service("EVAL", "POLICY", system)

    print("")
    print("*** FINAL RESULT: ***")
    for setop, gain, output in zip(result["setops"], result["performances"], result["outputs"]):
        nodes_before = setop.run_service("GET", "INPUT")[0].run_service("GET", "ACCESSED_NODES")
        nodes_after = output[1].run_service("GET", "ACCESSED_NODES")
        model = setop.run_service("GET", "PSETOP_MODEL", "USER_MODEL")
        prev_perf = model.run_service("EVAL", "INPUT", setop.run_service("GET", "INPUT"), ["SPEEDUP"])
        past_perf = model.run_service("EVAL", "OUTPUT", output, ["SPEEDUP"])
        gain = model.run_service("EVAL", "EDGE", setop.run_service("GET", "INPUT"), output, ["SPEEDUP"])

        print("Setop "+ setop.run_service("GET", "NAME") +":")
        print("     Nodes before: "+str([node.run_service("GET", "NAME") for node in nodes_before]))
        print("     Nodes after: "+str([node.run_service("GET", "NAME") for node in nodes_after]))
        print("     Speedup before: ", prev_perf["SPEEDUP"])
        print("     Speedup before: ", past_perf["SPEEDUP"])
        print("     Speedup gain: ", gain["SPEEDUP"])

    sleep(5)
    rc = system.run_service("MCA", "SHUTDOWN")
    if rc != DYNRM_MCA_SUCCESS:
        print("System Shutdown failed with ", rc)
        exit(1)
    

if __name__ == "__main__":
    print()
    print("================================================================================")
    print("Starting MCA Test 'policy'")
    print("================================================================================")


    test1(Discrete_Steepest_Ascend,
          MCASystemModule, 
          join(dirname(dirname(__file__)), "test_input/topology/10_node_system.yaml"),
          join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission_policy.py")
    )

    test1(Discrete_Steepest_Ascend,
          PrrteSystem, 
          join(dirname(dirname(__file__)), "test_input/topology/8_node_system.yaml"),
          join(dirname(dirname(__file__)), "test_input/submission/task_graph_submission_policy.py")
    )

    print()
    print("================================================================================")
    print("Finished MCA Test 'policy' successfully")
    print("================================================================================")



'''
    nodes1 = copy(nodes[:1])
    job1 = Job("job1")
    job1.add_node_names([node.name for node in nodes1])
    params1 = {"t_s": 1, "t_p": 300}
    mappings1 = {"full_nodes": lambda res_block: (sum(node.num_slots for node in res_block["nodes"]), None)}
    constraints1 = {"min": lambda state: state.size >= 8, 
                   "max": lambda state: state.size <= 32,
                   "power_of_two": lambda state: (state.size & (state.size - 1)) == 0}
    setop1 = create_amdahl_grow_setop("id1", nodes1, params1, mappings1, constraints1)
    job1.add_setop(setop1)

    nodes2 = copy(nodes[1:2])
    job2 = Job("job2")
    job2.add_node_names([node.name for node in nodes2])
    params2 = {"t_s": 15, "t_p": 300}
    mappings2 = {"full_nodes": lambda res_block: (sum(node.num_slots for node in res_block["nodes"]), None)}
    constraints2 = {"min": lambda state: state.size >= 1, 
                   "max": lambda state: state.size <= 40}
    setop2 = create_amdahl_grow_setop("id2", nodes2, params2, mappings2, constraints2)
    job2.add_setop(setop2)

    nodes3 = copy(nodes[2:3])
    job3 = Job("job3")
    job3.add_node_names([node.name for node in nodes3])
    params3 = {"t_s": 2, "t_p": 300}
    mappings3 = {"full_nodes": lambda res_block: (sum(node.num_slots for node in res_block["nodes"]), None)}
    constraints3 = {"min": lambda state: state.size > 1, 
                   "max": lambda state: state.size <= 16}
    setop3 = create_amdahl_grow_setop("id3", nodes3, params3, mappings3, constraints3)
    job3.add_setop(setop3)

    system.add_jobs([job1, job2, job3])
'''