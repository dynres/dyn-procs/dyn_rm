
================================================================================
Starting MCA Test 'topology_creation'
================================================================================

Testing module DefaultTaskGraphCreationModule

========= TASK GRAPH ========
TASK: MCAGraphObject://0 (MCAGraphObject://0)
====> STATUS: 0
====> PREDECESSORS: ['task1', 'task2']
====> SUCCESSORS: []
TASK: MCAGraphObject://0/1 (MCAGraphObject://0/1)
====> STATUS: 0
====> PREDECESSORS: []
====> SUCCESSORS: ['submission_1', 'task2']
TASK: MCAGraphObject://0/2 (MCAGraphObject://0/2)
====> STATUS: 0
====> PREDECESSORS: ['task1']
====> SUCCESSORS: ['submission_1']
=================================


================================================================================
Finished MCA Test 'topology_creation' successfully
================================================================================
