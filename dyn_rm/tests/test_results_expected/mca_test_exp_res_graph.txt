
=================== Start of MCA Test: 'Graph' =================


############ Vertices ##########

Vertex1: setting name to 'v1'
Vertex1: setting name to 'v1' returned 'SUCCESS'

Vertex1: getting name
Vertex1: getting name returned: v1

Vertex1: Setting attribute: 'attribute_key1' : 'attribute_val1'
Vertex1: Setting attribute: 'attribute_key1' : 'attribute_val1' returned 'SUCCESS'

Vertex1: Setting monitoring entry: 'timestamp1' : 'monitoring_entry1'
Vertex1: Setting Setting monitoring entry: 'timestamp1' : 'monitoring_entry1' returned 'SUCCESS'

Vertex1: Setting monitoring entry: 'timestamp2' : 'monitoring_entry2'
Vertex1: Setting Setting monitoring entry: 'timestamp2' : 'monitoring_entry2' returned 'SUCCESS'

Vertex1: Getting monitoring entry1
Vertex1: Getting Setting monitoring entry1 returned: monitoring_entry1

Vertex1: Removing monitoring entry1
Vertex1:Removing monitoring entry1 returned 'SUCCESS'

Vertex1: Getting monitoring data
Vertex1: Getting monitoring data returned {'timestamp2': 'monitoring_entry2'}

Creating Vertices 'v2' and 'v3'


############ Edges ##########


Edge1: Setting name to 'e1'
Edge1: Setting name to 'e1' returned 'SUCCESS'

Edge1: Setting input to '[v1]'
Edge1: Setting input to '[v1]' returned 'SUCCESS'

Edge1: Setting output to '[v2,v3]'
Edge1: Setting input to '[v1]' returned 'SUCCESS'

Edge1: Setting ATTRIBUTE: 'e1_attribute_key' : 'e1_attribute_val'
Edge1: Setting ATTRIBUTE: 'e1_attribute_key' : 'e1_attribute_val' returned 'SUCCESS'

Setting up edge 'e2'

Setting up edge in vertices
Vertex1 get edge e1: e1
Vertex1 get edges: ['e2', 'e1']
Vertex1 get in_edges: ['e2']
Vertex1 get out_edges: ['e1']
Vertex1 edge filter e1_attribute_key: ['e1']
Vertex1 edge filter 'v3' in INPUT': ['e2']


############ Graphs ##########


Graph1: Adding vertices '[vertex1, vertex2, vertex3]'

Graph1: Adding Edges '[e1, e2]'

Graph1: Get vertex 'v1'
Graph1: Get vertex 'v1' returned v1

Graph1: Get vertices-by_filter name=='v1'
Graph1: Get vertices-by_filter name=='v1' returned ['v1']

Graph1: Get edges-by_filter 'e1_attribute_key'
Graph1: Get edges-by_filter 'e1_attribute_key' returned ['e1']

graph1: 'GET', 'ALL_GRAPH_VERTICES'
graph1: 'GET', 'ALL_GRAPH_VERTICES' returned ['g1', 'v1', 'v2', 'v3']
vertex g1:
==> In_Edges: []
==> Out_Edges: []
vertex v1:
==> In_Edges: ['e2']
==> Out_Edges: ['e1']
vertex v2:
==> In_Edges: ['e1']
==> Out_Edges: []
vertex v3:
==> In_Edges: ['e1']
==> Out_Edges: ['e2']

graph1: 'GET', 'ALL_GRAPH_EDGES'
graph1: 'GET', 'ALL_GRAPH_EDGES' returned ['e1', 'e2']
edge e1:
==> Input: ['v1']
==> Output: ['v2', 'v3']
edge e2:
==> Input: ['v3']
==> Output: ['v1']


############ Vertex Copy ##########


Vertex1: GET COPY
Vertex1: GET COPY returned: name=v1 4

Vertex1_copy: REMOVE EDGE 'e1'

graph1: 'UPDATE' 'GRAPH' [v1_copy], []
graph1: 'UPDATE' 'GRAPH' [v1_copy], [] returned 'SUCCESS'

graph1: 'GET', 'ALL_GRAPH_VERTICES'
graph1: 'GET', 'ALL_GRAPH_VERTICES' returned ['g1', 'v1', 'v2', 'v3']
vertex g1:
==> In_Edges: []
==> Out_Edges: []
vertex v1:
==> In_Edges: ['e2']
==> Out_Edges: []
vertex v2:
==> In_Edges: []
==> Out_Edges: []
vertex v3:
==> In_Edges: []
==> Out_Edges: ['e2']

graph1: 'GET', 'ALL_GRAPH_EDGES'
graph1: 'GET', 'ALL_GRAPH_EDGES' returned ['e2']
edge e2:
==> Input: ['v3']
==> Output: ['v1']
