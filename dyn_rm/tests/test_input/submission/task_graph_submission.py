from dyn_rm.mca.base.system.module.tasks.task import MCATaskModule
from dyn_rm.mca.base.system.module.psets.pset import MCAPSetModule
from dyn_rm.mca.base.system.module.psets.proc import MCAProcModule
from dyn_rm.mca.base.system.module.topology.node import MCANodeModule
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel

from functools import partial

# 1 proc per node, multiple of 2 and minimum 4 nodes
def output_space_generator1(setop, input, graphs, task=None):

    null_pset = input[0]
    #task_graph = graphs["TASK_GRAPH"]
    #pset_graph = graphs["PSET_GRAPH"]
    topology_graph = graphs["TOPOLOGY_GRAPH_ADD"]

    nodes = topology_graph.run_service("GET", "GRAPH_VERTICES_BY_FILTER", lambda x: isinstance(x, MCANodeModule))

    # multiple of 2 constraint
    if (len(nodes) & (len(nodes) - 1)) != 0:
        print("===> Multiple of 2 constraint not satisfied: "+str(len(nodes)))
        return [], []
    
    # minimum constraint
    if len(nodes) < 4:
        print("===> Minimum constraint not satisfied")
        return [], []
    
    
    procs = []
    # one proc per node
    for index in range(len(nodes)):
        cores = nodes[index].run_service("GET", "CORES")
        proc = MCAProcModule(str(index), task.run_service("GET", "TASK_EXECUTABLE"))
        proc.run_service("SET", "CORE_ACCESS", cores)
        procs.append(proc)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": 10, "t_p": 300})
    
    delta_pset = MCAPSetModule("delta", procs)
    delta_pset.run_service("SET", "TASK", task)
    delta_pset.run_service("ADD", "PSET_MODEL", "USER_MODEL", amdahl_model)

    print("LAUNCH OUPUT SPACE GENERATED for task " + task.run_service("GET", "NAME"))
    # Note: Lists of Lists => For each possibility return: output_lists and lists_of_adapted_objects 
    return [[delta_pset]], [[delta_pset] + procs]

def create_task_graph_function(task_graph, params):
    task1 = MCATaskModule("task1", "./mpi_program1 arg1")
    task2 = MCATaskModule("task2", "./mpi_program2 arg2")

    task1.run_service("SET", "ATTRIBUTE", "key1", "value1")
    task2.run_service("SET", "ATTRIBUTE", "key2", "value2")

    task1.run_service("SET", "TASK_LAUNCH_OUTPUT_SPACE_GENERATOR", partial(output_space_generator1, task=task1))
    task2.run_service("SET", "TASK_LAUNCH_OUTPUT_SPACE_GENERATOR", partial(output_space_generator1, task=task2))

    task_graph.run_service("ADD", "TASKS", [task1, task2])
    task_graph.run_service("MAKE", "TASK_DEPENDENCY", task1, task2)

    return task_graph