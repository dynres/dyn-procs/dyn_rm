from dyn_rm.mca.base.system.module.tasks.task import MCATaskModule
from dyn_rm.mca.base.system.module.psets.pset import MCAPSetModule
from dyn_rm.mca.base.system.module.psets.proc import MCAProcModule
from dyn_rm.mca.base.system.module.topology.node import MCANodeModule
from dyn_rm.mca.system.modules.psets.pset_models.amdahl import AmdahlPsetModel

from functools import partial

from os.path import join,dirname

def get_delta_pset_for_nodes(nodes, task):
    procs = []
    # fill node
    for index in range(len(nodes)):
        cores = nodes[index].run_service("GET", "CORES")
        for core in cores:
            proc = MCAProcModule(str(index), task.run_service("GET", "TASK_EXECUTABLE"))
            proc.run_service("SET", "CORE_ACCESS", cores)
            procs.append(proc)
    
    delta_pset = MCAPSetModule("delta", procs)
    delta_pset.run_service("SET", "TASK", task)

    return delta_pset, procs
# 1 proc per node, multiple of 2 and minimum 4 nodes
def output_space_generator1(setop, input, graphs, task=None, t_s=0, t_p=1, num_nodes=1):

    topology_graph = graphs["TOPOLOGY_GRAPH_ADD"]

    nodes = topology_graph.run_service("GET", "GRAPH_VERTICES_BY_FILTER", lambda x: isinstance(x, MCANodeModule))

    # minimum constraint
    if len(nodes) != num_nodes:
        print("===> Fixed nodes constraint not satisfied")
        return [], []
    
    
    delta_pset, procs = get_delta_pset_for_nodes(nodes, task)
    
    amdahl_model = AmdahlPsetModel()
    amdahl_model.run_service("SET", "MODEL_PARAMS",{"t_s": t_s, "t_p": t_p})

    delta_pset.run_service("ADD", "PSET_MODEL", "USER_MODEL", amdahl_model)

    print("LAUNCH OUPUT SPACE GENERATED for task " + task.run_service("GET", "NAME"))

    # Note: Lists of Lists => For each possibility return: output_lists and lists_of_adapted_objects 
    return [[delta_pset]], [[delta_pset] + procs]

def create_task_graph_function(task_graph, params):
    task1 = MCATaskModule("task1", join(dirname(__file__), "executables", "DynMPISessions_v2a_release"))
    task2 = MCATaskModule("task2", join(dirname(__file__), "executables", "DynMPISessions_v2a_release"))
    task3 = MCATaskModule("task3", join(dirname(__file__), "executables", "DynMPISessions_v2a_release"))

    task1.run_service("SET", "TASK_EXECUTION_ARGUMENTS", ["-c 120", "-l 32 ", "-m i+", "-n 8", "-f 10"])
    task2.run_service("SET", "TASK_EXECUTION_ARGUMENTS", ["-c 120", "-l 8 ", "-m i+", "-n 8", "-f 10"])
    task3.run_service("SET", "TASK_EXECUTION_ARGUMENTS", ["-c 120", "-l 8 ", "-m i+", "-n 8", "-f 10"])

    task1.run_service("SET", "ATTRIBUTE", "key1", "value1")
    task2.run_service("SET", "ATTRIBUTE", "key2", "value2")
    task3.run_service("SET", "ATTRIBUTE", "key3", "value3")

    task1.run_service("SET", "TASK_LAUNCH_OUTPUT_SPACE_GENERATOR", partial(output_space_generator1, task=task1, t_s=1, t_p=300, num_nodes=1))
    task2.run_service("SET", "TASK_LAUNCH_OUTPUT_SPACE_GENERATOR", partial(output_space_generator1, task=task2, t_s=15, t_p=300, num_nodes=1))
    task3.run_service("SET", "TASK_LAUNCH_OUTPUT_SPACE_GENERATOR", partial(output_space_generator1, task=task3, t_s=2, t_p=300, num_nodes=1))

    task_graph.run_service("ADD", "TASKS", [task1, task2, task3])

    return task_graph