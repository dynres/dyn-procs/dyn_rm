import subprocess
import os
import filecmp

all_tests = [
    "connection",
    "event_loop",
    "graph",
    "setop_model",
    "submission",
    "task_graph_creation",
    "topology_creation",
    "vertex_model",
]


def run_tests(tests):

    print()
    print("============= MCA TEST: START ===============")

    failed_tests = []
    # Run the Python file
    for test in tests:
        filename = os.path.join("test_files", "mca_test_"+test+".py")
        results_file = os.path.join("test_results", "mca_test_res_"+test+".txt")
        expected_results_file = os.path.join("test_results_expected", "mca_test_exp_res_"+test+".txt")
        result = subprocess.run(['python3', os.path.join(filename)], capture_output=True, text=True)

        # Check if the process was successful
        with open(results_file, 'w') as file:
            file.write(result.stdout)

        ident = filecmp.cmp(results_file, expected_results_file, shallow=False)
        if result.returncode == 0 and ident:
            print(f"Successfully ran test: {test}")
        else:
            errors = ""
            if result.returncode != 0:
                errors+=" |non-zero returncode"+str(result.returncode)+"("+result.stderr+")|"
            if not ident:
                errors+=" |wrong output|"
            print(f"Error running {test}: "+errors)
            failed_tests.append(test)
    
    print()
    print("============= MCA TEST: RESULT ===============")
    print("SUCCESS:"+ str(len(tests) - len(failed_tests))+"/"+str(len(tests)))
    print("FAILURE:"+ str(len(failed_tests))+"/"+str(len(tests)))
    for failed_test in failed_tests:
        print("==== Test '"+failed_test+"' failed")
    print("==============================================")
    print()

if __name__ == "__main__":
    run_tests(all_tests)