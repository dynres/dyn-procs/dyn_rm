from .amdahl import *
from .inverse_amdahl import *
from .null import *
from .constant import *
from .linear import *