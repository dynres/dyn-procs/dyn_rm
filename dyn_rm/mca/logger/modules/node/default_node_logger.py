from dyn_rm.mca.base.logger.module import MCALoggerModule
import time

class DefaultNodeLogger(MCALoggerModule):
    filename = "node.csv"

    def get_header(self) -> list:
        return ['timestamp', 'node_id', 'event', 'num_cores', 'num_free_cores']

    def create_event_function(self, ev, node):
        running_jobs = dict()
        running_procs = dict()
        for proc in node.proc_names.keys():
            running_jobs[proc.split(":")[0]] = proc.split(":")[0]
            if not proc.split(":")[0] in running_procs:
                running_procs[proc.split(":")[0]] = 0
            running_procs[proc.split(":")[0]] += 1
        
        event = dict()
        event["timestamp"] = time.time()
        event["event"] = ev
        event["node_id"] = node.run_service("GET", "NAME")
        event["num_cores"] = node.run_service("GET", "NUM_CORES")
        event["num_free_cores"] = node.run_service("GET", "NUM_FREE_CORES")


        return event

    def log_event_function(self, event: dict):
        row = [event["timestamp"], event['node_id'], event['event'], event["num_cores"], event["num_free_cores"]]
        self.write_rows(self.get_filename(), [row]) 

    def log_events_function(self, events: list):
        rows = []
        for event in events:
            row = [event["timestamp"], event['node_id'], event['event'], event["num_cores"], event["num_free_cores"]]
            rows.append(row)
            
        self.write_rows(self.get_filename(), rows)               

    def postprocessing_function(self, params: dict):
        pass
    
    def preprocessing_function(self, params: dict):
        pass